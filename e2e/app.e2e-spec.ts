import { RebomPage } from './app.po';

describe('rebom App', () => {
  let page: RebomPage;

  beforeEach(() => {
    page = new RebomPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
