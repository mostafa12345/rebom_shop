import { AppConfig } from './../../config/const.config';
import {AppService} from './../../providers/app.service';
import {Component, OnInit} from '@angular/core';

@Component({selector: 'app-home-panel', templateUrl: './home-panel.component.html', styleUrls: ['./home-panel.component.css']})
export class HomePanelComponent implements OnInit {

  topHit : any[] = [];
  newProduct : any[] = [];
  constructor(private appService : AppService) {}

  ngOnInit() {
    this
      .appService
      .getHomeTopHit(0, 6)
      .then(val => {
        this.topHit = val;
      });
    this
      .appService
      .getNewProduct(0, 6)
      .then(val => {
        this.newProduct = val;
      });
  }
  imageUrl(item) {
    return AppConfig.BASE_PATH+"/images/" + item;
  }
}
