import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import { AppConfig } from '../config/const.config';

@Component({selector: 'app-panel', templateUrl: './panel.component.html', styleUrls: ['./panel.component.css']})
export class PanelComponent implements OnInit {

  user : any = {};
  constructor(private router : Router) {
  }

  ngOnInit() {
    this.user= AppConfig.user();
  }
}
