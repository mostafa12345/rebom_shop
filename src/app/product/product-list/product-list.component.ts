import {CartService} from './../../providers/cart.service';
import {Component, OnInit} from '@angular/core';
import {AppService} from '../../providers/app.service';
import {trigger, transition, style, animate, state} from "@angular/animations";
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class ProductListComponent implements OnInit {

  message : any;
  categoryid : number;
  products : any[] = [];
  category : any;
  limit : number = 12;
  search : string;
  loading : boolean = false;
  constructor(private appService : AppService, private activateRoute : ActivatedRoute, private cartService : CartService) {}

  ngOnInit() {
    this
      .activateRoute
      .params
      .subscribe(params => {
        this.categoryid = params['id'];
        this.search = params['word'];
        this.loading = true;
        if (this.search) {
          this
            .appService
            .getSearch(0, this.limit, this.search)
            .then(val => {
              this.products = val;
              this.loading = false;
            });
        } else {
          this
            .appService
            .getProducts(this.categoryid, 0, this.limit, null)
            .then(val => {
              this.products = val;
              this.loading = false;
            });
          if (this.categoryid) {
            this
              .appService
              .getCategory(this.categoryid)
              .then(val => {
                this.category = val;
              });
          }
        }
      });
  }

  onScroll() {
    this.loading = true;
    if (this.search) {
      this
        .appService
        .getSearch(this.products.length, this.limit, this.search)
        .then(val => {
          this.products = val;
          this.loading = false;
        });
    } else {
      this
        .appService
        .getProducts(this.categoryid, this.products.length, this.limit, null)
        .then(val => {
          this.products = val;
          this.loading = false;
        });
    }
  }

  imageUrl(item) {
    return "http://localhost:8080/marketing/images/" + item;
  }

  addToCart(product : any) {
    if (!this.hasInCart(product)) {
      let item = {
        product: {
          id: product.id,
          title: product.title,
          price: (product.price - product.discount * product.price / 100)
        },
        num: 1
      };
      this
        .cartService
        .addToCart(item);
    } else {
      this
        .cartService
        .removeFromCart(product.id);
    }
  }

  hasInCart(item : any) : boolean {
    return this
      .cartService
      .hasInCart(item.id);
  }
}
