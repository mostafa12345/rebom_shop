import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  HostListener
} from '@angular/core';
import {AppService} from '../../providers/app.service';
import {trigger, transition, style, animate, state} from "@angular/animations";
import {ActivatedRoute} from '@angular/router';
import {CartService} from '../../providers/cart.service';
declare var $ : any;
@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class ProductPageComponent implements OnInit {

  message : any;
  productId : number;
  product : any;
  @ViewChild("carousel")carousel : ElementRef;
  comments : any[];

  constructor(private appService : AppService, private routeState : ActivatedRoute, private cartService : CartService) {}

  ngOnInit() {
    this
      .routeState
      .params
      .subscribe(params => {
        this.productId = params['id'];
        if (this.productId != null) {
          this
            .appService
            .getProduct(this.productId)
            .then(val => {
              this.product = val;
              this.comments = this.product.comments;
              this.carouselInit();
            })
        } else {
          this.message = {
            title: "خطای ۴۰۴!",
            content: "صفحه مورد نظر یافت نشد."
          }
        }
      });
  }

  carouselInit() {
    setTimeout(() => {
      let w = $(this.carousel.nativeElement).width();
      let r = w / 800;
      $(this.carousel.nativeElement).css('height', r * 600);
      $(this.carousel.nativeElement).slider({
        indicators: false,
        height: r * 600
      });
      $('ul.tabs').tabs();
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    let w = $(this.carousel.nativeElement).width();
    let r = w / 800;
    $(this.carousel.nativeElement).css('height', r * 600);
    $(this.carousel.nativeElement).slider({
      indicators: false,
      height: r * 600
    });
  }

  next() {
    $(this.carousel.nativeElement).slider('next');
  }
  prev() {
    $(this.carousel.nativeElement).slider('prev');
  }
  ngAfterViewInit() {}

  imageUrl(item) {
    return "http://localhost:8080/marketing/images/" + item;
  }

  addToCart() {
    if (!this.hasInCart()) {
      let item = {
        product: {
          id: this.product.id,
          title: this.product.title
        },
        num: 1
      };
      this
        .cartService
        .addToCart(item);
    } else {
      this
        .cartService
        .removeFromCart(this.product.id);
    }
  }

  hasInCart() : boolean {
    return this
      .cartService
      .hasInCart(this.product.id);
  }
}
