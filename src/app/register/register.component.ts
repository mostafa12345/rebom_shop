import {Router} from '@angular/router';
import {AppConfig} from './../config/const.config';
import {AuthService} from './../providers/auth.service';
import {trigger, transition, style, animate, state} from "@angular/animations";
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
declare var $ : any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class RegisterComponent implements OnInit {

  regForm : FormGroup;
  province : any[] = [];
  cities : any[] = [];
  message : any;
  loading : boolean = false;
  constructor(private fb : FormBuilder, private authService : AuthService, private router : Router) {}

  ngOnInit() {
    this.regForm = this
      .fb
      .group({
        'name': [
          '', Validators.required
        ],
        'family': [
          '', Validators.required
        ],
        'email': [''],
        'mobile': [
          '', Validators.required
        ],
        'phone': [''],
        'zip': [
          '', Validators.required
        ],
        'address': [
          '', Validators.required
        ],
        'province': [
          '', Validators.required
        ],
        'city': [
          '', Validators.required
        ],
        'passwords': this
          .fb
          .group({
            'password': [
              '', Validators.required
            ],
            'repassword': ['', Validators.required]
          }, {validator: this.areEqual})
      });
    this
      .authService
      .getProvince()
      .then(val => {
        this.province = val;
      });
  }

  ngAfterViewInit() {}

  provinceSelected(val) {
    let v = val.split(':');
    if (v && v.length > 1) {
      this.cities = this.province[v[0]].cities;
    }
  }

  submit() {
    if (this.regForm.valid) {
      this.loading = true;
      let value = $.extend(true, {}, this.regForm.value);
      delete value.province.cities;
      value.password = value.passwords.password;
      delete value.passwords;
      this
        .authService
        .register(value)
        .then(val => {
          if (val.result == 'success') {
            AppConfig.storeToken(val.token);
            AppConfig.storeUser(val);
            if (AppConfig.path!=null) {
              let path=AppConfig.path;
              AppConfig.storePath(null);
              setTimeout(() => {
                this
                  .router
                  .navigate([path]);
              });
            }
            this
              .regForm
              .reset();
          } else {
            this.message = val;
          }
          this.loading = false;
        });
    }
  }

  areEqual(group : FormGroup) {
    let val;
    let valid = true;
    for (let name in group.controls) {
      if (val === undefined) {
        val = group.controls[name].value
      } else {
        if (val !== group.controls[name].value) {
          valid = false;
          break;
        }
      }
    }
    if (valid) {
      return null;
    }
    return {areEqual: true};
  }
}
