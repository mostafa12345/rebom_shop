import {Component, OnInit} from '@angular/core';
import { AppService } from '../providers/app.service';

@Component({selector: 'app-left-side', templateUrl: './left-side.component.html', styleUrls: ['./left-side.component.css']})
export class LeftSideComponent implements OnInit {

  categories : any[];
  topHit : any[] = [];
  newProduct : any[] = [];
  constructor(private appService : AppService) {}

  ngOnInit() {
    this
      .appService
      .getHomeCategories()
      .then(val => {
        this.categories = val;
      });
    this
      .appService
      .getHomeTopHit(0, 6)
      .then(val => {
        this.topHit = val;
      });
    this
      .appService
      .getNewProduct(0, 6)
      .then(val => {
        this.newProduct = val;
      });
  }
  imageUrl(item) {
    return "http://localhost:8080/marketing/images/" + item;
  }
}
