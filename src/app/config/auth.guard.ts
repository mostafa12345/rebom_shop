import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot ,CanDeactivate} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../providers/auth.service';
import { AppConfig } from './const.config';
import {Router} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate{
  constructor(private authService:AuthService,private router:Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(AppConfig.token()!=null)
      return true;
    this.router.navigate(['/login']);
  }
}

@Injectable()
export class GaustGuard implements CanActivate{
  constructor(private authService:AuthService,private router:Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(AppConfig.token()==null)
      return true;
    this.router.navigate(['']);
  }
}
