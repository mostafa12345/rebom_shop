declare function unescape(s : string) : string;
declare function escape(s : string) : string;
import {SessionStorageService} from 'ngx-webstorage';

export class AppConfig {
  static _cookieService = new SessionStorageService();
  public static BASE_PATH : string = "http://localhost:8080/marketing";

  public static token = function () : string {
    return AppConfig
      ._cookieService
      .retrieve("user_token");
  };

  public static user = function () : any {
    let baseValue: string = AppConfig
      ._cookieService
      .retrieve("user");
    if (!baseValue)
      return null;
    let plainValue = AppConfig.b64_to_utf8(baseValue);
    let user: any = JSON.parse(plainValue);
    return user;
  };

  public static path() {
    return AppConfig
      ._cookieService
      .retrieve("path");
  }

  public static storeUser(user : any) {
    AppConfig
      ._cookieService
      .store("user", AppConfig.utf8_to_b64(JSON.stringify(user)));
  }

  public static storePath(path : string) {
    AppConfig
      ._cookieService
      .store("path", path);
  }

  public static storeToken(content : any) {
    AppConfig
      ._cookieService
      .store("user_token", content)
  }

  public static removeSession() {
    AppConfig
      ._cookieService
      .clear();
  }

  public static utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
  }

  public static b64_to_utf8(str) {
    return decodeURIComponent(escape(window.atob(str)));
  }
}
