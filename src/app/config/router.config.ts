import { AuthGuard } from './auth.guard';
import { ValidProfileComponent } from './../cart-page/valid-profile/valid-profile.component';
import { ProfileComponent } from './../panel/profile/profile.component';
import { MessagesComponent } from './../panel/messages/messages.component';
import {OrderListComponent} from './../panel/order-list/order-list.component';
import {LoginComponent} from './../login/login.component';
import {ProductPageComponent} from './../product/product-page/product-page.component';
import {NewsPageComponent} from './../news/news-page/news-page.component';
import {Routes} from '@angular/router';
import {HomeComponent} from "../home/home.component";
import {PageComponent} from "../page/page.component";
import {NewsListComponent} from '../news/news-list/news-list.component';
import {ProductListComponent} from '../product/product-list/product-list.component';
import {CategoryComponent} from '../category/category.component';
import {CartPageComponent} from '../cart-page/cart-page.component';
import {RegisterComponent} from '../register/register.component';
import {PanelComponent} from '../panel/panel.component';
import {Component} from '@angular/core';
import { HomePanelComponent } from '../panel/home-panel/home-panel.component';

export const appRoutes : Routes = [
  {
    path: '',
    component: HomeComponent
  }, {
    path: 'panel',
    component: PanelComponent,
    canActivate:[AuthGuard],
    children: [
      {
        path: '',
        component: HomePanelComponent
      }, {
        path: 'order-list',
        component: OrderListComponent
      }, {
        path: 'profile',
        component: ProfileComponent
      },{
        path: 'messages',
        component: MessagesComponent
      }
    ]
  },{
    path: 'validation',
     canActivate:[AuthGuard],
    component: ValidProfileComponent
  }, {
    path: 'products',
    component: CategoryComponent
  }, {
    path: 'register',
    component: RegisterComponent
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'news',
    component: NewsListComponent
  }, {
    path: 'news/:id',
    component: NewsPageComponent
  }, {
    path: 'cart',
    component: CartPageComponent
  }, {
    path: ':page',
    component: PageComponent
  }, {
    path: 'products/:id',
    component: ProductListComponent
  }, {
    path: 'search/:word',
    component: ProductListComponent
  }, {
    path: 'product/:id',
    component: ProductPageComponent
  }
];
