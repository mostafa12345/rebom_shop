import {Router} from '@angular/router';
import {AppConfig} from './../config/const.config';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {trigger, transition, style, animate, state} from "@angular/animations";
import {Component, OnInit} from '@angular/core';
import {AuthService} from '../providers/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class LoginComponent implements OnInit {

  message : any;
  form : FormGroup;
  loading : boolean = false;
  constructor(private fb : FormBuilder, private authService : AuthService, private router : Router) {}

  ngOnInit() {
    this.form = this
      .fb
      .group({
        'username': [
          '', Validators.required
        ],
        'password': [
          '', Validators.required
        ],
        'remember': ['']
      });
  }
  submit() {
    if (this.form.valid) {
      this.loading = true;
      this
        .authService
        .login(this.form.value)
        .then(val => {
          if (val.result == 'success') {
            AppConfig.storeToken(val.token);
            AppConfig.storeUser(val)
            if (AppConfig.path!=null) {
              let path=AppConfig.path;
              AppConfig.storePath(null);
              setTimeout(() => {
                this
                  .router
                  .navigate([path]);
              });
            }
            this
              .form
              .reset();
          } else {
            this.message = val;
          }
          this.loading = false;
        })
    }
  }
}
