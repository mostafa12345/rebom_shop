import {AuthService} from './../providers/auth.service';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {trigger, transition, style, animate, state} from "@angular/animations";
import {AppConfig} from '../config/const.config';

import '../../assets/js/persian-date.js';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class EditProfileComponent implements OnInit {

  page : number = 1;
  user : any = {};
  passform : FormGroup;
  form : FormGroup;
  province : any[] = [];
  cities : any[] = [];
  message : any;
  loading : boolean = false;
  constructor(private fb : FormBuilder, private authService : AuthService, private Router : Router) {}

  ngOnInit() {
    this.user = AppConfig.user();
    if (!this.user) {
      setTimeout(() => {
        this
          .Router
          .navigate(['login']);
      });
    }
    this.passform = this
      .fb
      .group({
        'oldPassword': [
          '', Validators.required
        ],
        'passwords': this
          .fb
          .group({
            'password': [
              '', Validators.required
            ],
            'repassword': ['', Validators.required]
          }, {validator: this.areEqual})
      });
    this.form = this
      .fb
      .group({
        'name': [
          this.user.name, Validators.required
        ],
        'family': [
          this.user.family, Validators.required
        ],
        'email': [this.user.email],
        'birthDay': [this.user.birthDay],
        'expert': [this.user.expert],
        'sex': [this.user.sex],
        'type': [this.user.type],
        'mobile': [
          this.user.mobile, Validators.required
        ],
        'phone': [this.user.phone],
        'zip': [
          this.user.zip, Validators.required
        ],
        'address': [
          this.user.address, Validators.required
        ],
        'province': [
          this.user.province, Validators.required
        ],
        'city': [this.user.city, Validators.required]
      });
    this
      .authService
      .getProvince()
      .then(val => {
        this.province = val;
        for (let i = 0; i < this.province.length; i++) {
          if (this.province[i].id == this.user.province.id) {
            this.provinceSelected(i + ":d");
          }
        }
      });
  }

  ngAfterViewInit() {}

  openDatePicker() {
    $('.date-picher-modal').show();
  }

  onSelect(val) {
    this
      .form
      .controls['birthDay']
      .setValue(val);
    $('.date-picher-modal').hide();
  }

  provinceSelected(val) {
    let v = val.split(':');
    console.log(v)
    if (v && v.length > 1) {
      this.cities = this.province[v[0]].cities;
    }
  }

  submitEdit() {}
  areEqual(group : FormGroup) {
    let val;
    let valid = true;
    for (let name in group.controls) {
      if (val === undefined) {
        val = group.controls[name].value
      } else {
        if (val !== group.controls[name].value) {
          valid = false;
          break;
        }
      }
    }
    if (valid) {
      return null;
    }
    return {areEqual: true};
  }
}
