import {Component, OnInit} from '@angular/core';
import {AppService} from '../../providers/app.service';
import {trigger, transition, style, animate, state} from "@angular/animations";
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class NewsListComponent implements OnInit {
  items : any[]=[];
  limit : number = 5;
  loading : boolean = false;

  constructor(private appService : AppService) {}

  ngOnInit() {
    this.loading = true;
    this
      .appService
      .getNewsList(this.items.length,this.limit)
      .then(val => {
        this.items = val;
        this.loading = false;
      });
  }

  onScroll() {
    this.loading = true;
    this
      .appService
      .getNewsList(this.items.length,this.limit)
      .then(val => {
        this.items = val;
        this.loading = false;
      });
  }

  hasNews(){
    return this.appService.newsHasItems;
  }
}
