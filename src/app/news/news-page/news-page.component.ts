import {Component, OnInit} from '@angular/core';
import {AppService} from '../../providers/app.service';
import {ActivatedRoute} from '@angular/router';
import {trigger, transition, style, animate, state} from "@angular/animations";

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class NewsPageComponent implements OnInit {

  news:any;
  constructor(private appService : AppService,private activateRoute:ActivatedRoute) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params=>{
      if(params['id']){
        this.appService.getNews(params['id']).then(val => {
                this.news = val;
              });
      }else{
        this.news={
          title:"خطای ۴۰۴!",
          content:"صفحه مورد نظر یافت نشد."
        };
      }
    });
  }

}
