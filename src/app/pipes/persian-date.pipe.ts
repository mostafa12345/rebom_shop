import {Pipe, PipeTransform} from '@angular/core';
import {PersianCalendarServiceService} from '../providers/persian-calendar-service.service';

@Pipe({name: 'persianDate'})
export class PersianDatePipe implements PipeTransform {

  constructor(private persianService : PersianCalendarServiceService) {}
  transform(value : any, args?: any) : any {
    if(value != null) {
      return this.persianService
        .PersianCalendar(new Date(value));
    }
    return null;
  }

}
