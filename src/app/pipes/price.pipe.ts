import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'price'
})
export class PricePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(!value){
      return 0;
    }
    console.log(args)
    if(args){
      return value-(value*args/100);
    }
  }

}
