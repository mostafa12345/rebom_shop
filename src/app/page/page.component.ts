import {Component, OnInit} from '@angular/core';
import {AppService} from '../providers/app.service';
import {ActivatedRoute} from '@angular/router';
import {trigger, transition, style, animate, state} from "@angular/animations";

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class PageComponent implements OnInit {

  page : any;
  constructor(private appService : AppService, private activateRoute : ActivatedRoute) {}

  ngOnInit() {
    this
      .activateRoute
      .params
      .subscribe(params => {
        if (params['page']) {
          this
            .appService
            .getPage(params['page'])
            .then(val => {
              this.page = val;
            });
        } else {
          this.page = {
            title: "خطای ۴۰۴!",
            content: "صفحه مورد نظر یافت نشد."
          };
        }
      });
  }

}
