import {AppService} from './../providers/app.service';
import {trigger, transition, style, animate, state} from "@angular/animations";
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class CategoryComponent implements OnInit {

  categories : any[];
  height : number = 85;
  constructor(private appService : AppService) {}

  ngOnInit() {
    this
      .appService
      .getHomeCategories()
      .then(val => {
        this.categories = val;
      });
  }
  imageUrl(item) {
    return "http://localhost:8080/marketing/images/" + item;
  }

  getHeight(i : number) : number {
    return this.height - i*10;
  }
  getColWidth() : number {
    return 100 / (this.categories.length + 2);
  }
}
