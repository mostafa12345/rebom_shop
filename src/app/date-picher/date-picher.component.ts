import {Component, OnInit, Output, EventEmitter} from '@angular/core';
declare function persianDate(val : any);

@Component({selector: 'date-picher', templateUrl: './date-picher.component.html', styleUrls: ['./date-picher.component.css']})
export class DatePicherComponent implements OnInit {

  @Output()onSelect : EventEmitter < string >= new EventEmitter < string > ();
  date : any;
  dateSelected : any;
  day : number;
  week : number;
  daysInMonth : number;
  firstDayInWeek : number;
  daysInPrvMonth : number;
  currentMonth : number;
  month : number;
  currentYears : number;
  years : number;
  currentDay : number;
  days : string[] = [
    'جمعه',
    'شنبه',
    'یکشنبه',
    'دوشنبه',
    'سه شنبه',
    'چهار شنبه',
    'پنچشنبه'
  ];
  monthNames : string[] = [
    "",
    "فروردین",
    "اردیبهشت",
    "خرداد",
    "تیر",
    "مرداد",
    "شهریور",
    "مهر",
    "آبان",
    "آذر",
    "دی",
    "بهمن",
    "اسفند"
  ];
  items : any[][] = [];
  hour : number[][] = [
    [
      1, 2, 3, 4
    ],
    [
      5, 6, 7, 8
    ],
    [
      9, 10, 11, 12
    ],
    [
      13, 14, 15, 16
    ],
    [
      17, 18, 19, 20
    ],
    [21, 22, 23, 24]
  ];
  minutes : number[][] = [
    [
      0, 5
    ],
    [
      10, 15
    ],
    [
      20, 25
    ],
    [
      30, 35
    ],
    [
      40, 45
    ],
    [50, 55]
  ];
  hourSelectd : number;
  minuteSelected : number = 0;
  yearsList : number[] = [];

  constructor() {}

  ngOnInit() {
    this.date = persianDate(new Date());
    this.initCalendar(this.date);
    this.currentYears = this
      .date
      .years();
    this.currentMonth = this
      .date
      .month();
    this.currentDay = this
      .date
      .date();
    this.hourSelectd = persianDate(new Date()).hour();
    this.minuteSelected = persianDate(new Date()).minute();
    if (this.minuteSelected % 5 != 0) {
      this.minuteSelected = this.minuteSelected - (this.minuteSelected % 5);
    }
    for (let i = this.currentYears; i >= 1310; i--) {
      this
        .yearsList
        .push(i);
    }

  }
  ngAfterViewInit() {
    // setTimeout(()=>{
    //   $('select').material_select();
    // })
  }
  initCalendar(pDate : any) {
    this.week = pDate.day();
    this.years = pDate.years();
    this.month = pDate.month();
    this.daysInMonth = pDate.daysInMonth();
    this.firstDayInWeek = pDate
      .startOf('month')
      .day();
    let lastmonth = pDate.subtract('months', 1);
    this.daysInPrvMonth = lastmonth.daysInMonth();
    let k = this.daysInPrvMonth - this.firstDayInWeek;
    let m = lastmonth.month();
    let filllLast = false;
    for (let i = 0; i < 6; i++) {
      this.items[i] = [];
      for (let j = 0; j < 7; j++) {
        if (!filllLast && k == this.daysInPrvMonth) {
          k = 0;
          m = lastmonth
            .add('months', 1)
            .month();
          filllLast = true;
        }
        k++;
        this.items[i][j] = {
          day: k,
          month: m
        };
        if (filllLast && this.daysInMonth == k) {
          k = 0;
          m = lastmonth
            .add('months', 2)
            .month();
        }
      }
    }
  }

  next() {
    this.date = this
      .date
      .add('month', 1);
    this.initCalendar(this.date);
  }

  prev() {
    this.date = this
      .date
      .subtract('month', 1);
    this.initCalendar(this.date);
  }

  yearsSelected(val:string) {
    let v = val.split(':');
    if (v && v.length > 1) {
      this.date=this.date.years(v[1]);
      this.initCalendar(this.date);
    }
  }
  monthSelected(val:string) {
    let v = val.split(':');
    if (v && v.length > 1) {
      this.date=this.date.month(val[0]);
      this.initCalendar(this.date);
    }
  }

  tody() {
    this.date = persianDate(new Date());
    this.initCalendar(this.date);
  }

  isToday(d : any) {
    return this.currentYears == this.years && this.currentMonth == d.month && this.currentDay == d.day;
  }

  onHourSelect(i : number) {
    this.hourSelectd = i;
  }

  onMinuteSelect(i : number) {
    this.minuteSelected = i;
  }

  onDateSelect(d : number) {
    this.dateSelected = persianDate([this.years, this.month, d])
    this
      .onSelect
      .emit(this.years + "/" + this.month + "/" + d);
  }

  isDateSelected(d : any) {
    if (this.dateSelected) {
      var b = persianDate([this.years, d.month, d.day]);
      if (this.dateSelected.diff(b, 'days') == 0) {
        return true;
      }
    }
    return false;
  }

}
