import {Component, Input, OnInit, AfterViewInit} from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  @Input("items") items: any[];
  @Input("url") url: string;
  @Input("defaultImage") defaultImage: string;
  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(()=>{
      $('.slider').slider();
    })
  }

  imageUrl(item){
    return this.url+"/"+item;
  }
}
