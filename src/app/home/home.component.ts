import {Component, AfterViewInit, ViewChild, ElementRef, OnInit} from '@angular/core';
import {AppService}  from '../providers/app.service';
import { CartService } from '../providers/cart.service';
declare var $: any;
declare var Materialize: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  home:any;
  topHits:any[];
  categories:any[];
  constructor(private appService: AppService, private cartService : CartService) { }

  ngOnInit() {
    this.appService.getHomeData().then(val => {
      this.home = val;
    });
    this.appService.getHomeTopHit(0,4).then(val => {
      this.topHits = val;
    });
    this.appService.getHomeCategories().then(val => {
      this.categories = val;
    });
  }

  ngAfterViewInit() {
    $('.parallax').parallax();
  }

  imageUrl(item){
    return "http://localhost:8080/marketing/images/"+item;
  }

   addToCart(product:any) {
    if (!this.hasInCart(product)) {
      let item = {
        product: {
          id: product.id,
          title: product.title
        },
        num: 1
      };
      this
        .cartService
        .addToCart(item);
    } else {
      this
        .cartService.removeFromCart(product.id);
    }
  }

  hasInCart(item:any) : boolean {
    return this
      .cartService
      .hasInCart(item.id);
  }
}
