import {Component, Input, OnInit, AfterViewInit, ViewChild} from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-news-slider',
  templateUrl: './news-slider.component.html',
  styleUrls: ['./news-slider.component.css']
})
export class NewsSliderComponent implements OnInit {

  @Input('items') items: any[];
  @ViewChild("carousel") carousel:any;

  constructor() {
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    setTimeout(()=>{
      $(this.carousel.nativeElement).carousel({fullWidth: true});
    })
  }
}
