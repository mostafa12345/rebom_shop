import {Router} from '@angular/router';
import {AppConfig} from './../../config/const.config';
import {Component, OnInit} from '@angular/core';

@Component({selector: 'app-valid-profile', templateUrl: './valid-profile.component.html', styleUrls: ['./valid-profile.component.css']})
export class ValidProfileComponent implements OnInit {

  user : any={};
  constructor(private router : Router) {
    this.user = AppConfig.user();
    if (!this.user) {
      setTimeout(() => {
        this
          .router
          .navigate(['login']);
      });
    }
  }

  ngOnInit() {}

  validateUser() {
    return this.user!=null && this.user.name!=null && this.user.family!=null && this.user.mobile!=null && this.user.province!=null && this.user.city!=null && this.user.zip!=null && this.user.address!=null;
  }
}
