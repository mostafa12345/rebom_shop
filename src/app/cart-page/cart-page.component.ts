import { Router } from '@angular/router';
import {CartService} from './../providers/cart.service';
import {Component, OnInit, Pipe} from '@angular/core';
import {trigger, transition, style, animate, state} from "@angular/animations";
import {ValuesPipe} from '../pipes/values.pipe';
import {LocalStorage, LocalStorageService} from 'ngx-webstorage';
import { AppConfig } from '../config/const.config';

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.css'],
  host: {
    '[@routeAnimation]': "'true'",
    '[style.display]': "'inline-block'",
    '[style.width]': "'100%'",
    '[style.height]': "'100%'"
  },
  animations: [trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate(300)
      ]),
      transition('* => void', animate(300, style({transform: 'translateX(100%)', opacity: 0})))
    ])]
})
export class CartPageComponent implements OnInit {

  carts : any = {};
  keys : string[] = [];
  constructor(private cartService : CartService, private storage : LocalStorageService,private router:Router) {}

  ngOnInit() {
    this.carts = this
      .storage
      .retrieve("carts");
    this.keys = Object.keys(this.carts);
    this
      .storage
      .observe("carts")
      .subscribe(val => {
        if (val) {
          this.carts = val;
          this.keys = Object.keys(this.carts);
        }
      });
  }
  remove(id : number) {
    this
      .cartService
      .removeFromCart(id);
  }
  getTotal() {
    let sum = 0;
    for (let key in this.carts) {
      sum += this.carts[key].product.price * this.carts[key].num;
    }
    return sum;
  }
}
