import {CartService} from './providers/cart.service';
import {Component, AfterViewInit, ViewChild, ElementRef, OnInit} from '@angular/core';
import {AppService} from './providers/app.service';
import {Router, NavigationEnd} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LocalStorage, LocalStorageService} from 'ngx-webstorage';
declare var $ : any;
declare var Materialize : any;

@Component({selector: 'app-root', templateUrl: './app.component.html', styleUrls: ['./app.component.css']})
export class AppComponent implements OnInit {
  @ViewChild('searchForm')searchForm : ElementRef;
  home : any;
  news : any[];
  contactForm : FormGroup;
  loading:boolean=false;
  cartCount : number = 0;
  search : string;
  categories : any[];
  constructor(private appService : AppService, private fb : FormBuilder, private router : Router, private storage : LocalStorageService, private cartService : CartService) {
    this.contactForm = this
      .fb
      .group({
        name: [
          "",
          [Validators.required]
        ],
        mobileNum: [
          "",
          [Validators.required]
        ],
        email: [""],
        content: [
          "",
          [Validators.required]
        ]
      });
    this
      .storage
      .observe("carts")
      .subscribe((val) => {
        if (val) {
          this.cartCount = Object.keys(val).length;
        }
      })
  }

  ngOnInit() {
    this
      .appService
      .getHomeData()
      .then(val => {
        this.home = val;
      });
    this
      .appService
      .getHomeNews(0, 4)
      .then(val => {
        this.news = val;
      });
    this
      .router
      .events
      .subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
          return;
        }
        document.body.scrollTop = 0;
      });
    this
      .appService
      .getHomeCategories()
      .then(val => {
        this.categories = val;
      });
  }

  ngAfterViewInit() {
    $(".button-collapse").sideNav({
      menuWidth: 300, // Default is 300
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
    });

    $(this.searchForm.nativeElement).hide();
  }

  openSearchForm() {
    $(this.searchForm.nativeElement).show();
  }

  closeSearchForm() {
    $(this.searchForm.nativeElement).hide();
    this.search = null;
  }

  searchSubmit() {
    this
      .router
      .navigate(['search', this.search]);
  }
  contactSubmit() {
    if (this.contactForm.valid) {
      this.loading=true;
      this
        .appService
        .sendMessage(this.contactForm.value)
        .then(val => {
          Materialize.toast(val.message, 6000);
          this
            .contactForm
            .reset();
            this.loading=false;
        }, (error) => {
          Materialize.toast("کاربر گرامی<br/>هم اکنون سرور پاسخگو نمی باشد.", 6000);
          this.loading=false;
        })
    }
  }

  imageUrl(item) {
    return "http://localhost:8080/marketing/images/" + item;
  }

  addToCart(product : any) {
    if (!this.hasInCart(product)) {
      let item = {
        product: {
          id: product.id,
          title: product.title
        },
        num: 1
      };
      this
        .cartService
        .addToCart(item);
    } else {
      this
        .cartService
        .removeFromCart(product.id);
    }
  }

  hasInCart(item : any) : boolean {
    return this
      .cartService
      .hasInCart(item.id);
  }

}
