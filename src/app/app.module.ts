import { AuthGuard } from './config/auth.guard';
import {appRoutes} from './config/router.config';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterializeModule} from 'ng2-materialize';
import {LazyLoadImageModule} from 'ng-lazyload-image';
import {AppService} from './providers/app.service';
import {RouterModule} from '@angular/router';
import {StarRatingModule} from 'angular-star-rating';
import {Ng2Webstorage} from 'ngx-webstorage';

import {AppComponent} from './app.component';
import {SliderComponent} from './home/slider/slider.component';
import {NewsSliderComponent} from './home/news-slider/news-slider.component';
import {PricePipe} from './pipes/price.pipe';
import {HomeComponent} from './home/home.component';
import {PageComponent} from './page/page.component';
import {NewsPageComponent} from './news/news-page/news-page.component';
import {PersianDatePipe} from './pipes/persian-date.pipe';
import {PersianCalendarServiceService} from './providers/persian-calendar-service.service';
import {NewsListComponent} from './news/news-list/news-list.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {ProductListComponent} from './product/product-list/product-list.component';
import {ProductPageComponent} from './product/product-page/product-page.component';
import { CartService } from './providers/cart.service';
import { LeftSideComponent } from './left-side/left-side.component';
import { CategoryComponent } from './category/category.component';
import { CartPageComponent } from './cart-page/cart-page.component';
import { ValuesPipe } from './pipes/values.pipe';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PanelComponent } from './panel/panel.component';
import { AuthService } from './providers/auth.service';
import { OrderListComponent } from './panel/order-list/order-list.component';
import { FactorComponent } from './cart-page/factor/factor.component';
import { PostComponent } from './cart-page/post/post.component';
import { ResultComponent } from './cart-page/result/result.component';
import { HomePanelComponent } from './panel/home-panel/home-panel.component';
import { MessagesComponent } from './panel/messages/messages.component';
import { ProfileComponent } from './panel/profile/profile.component';
import { ValidProfileComponent } from './cart-page/valid-profile/valid-profile.component';
import { ShowProfileComponent } from './show-profile/show-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { DatePicherComponent } from './date-picher/date-picher.component';

@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    NewsSliderComponent,
    PricePipe,
    HomeComponent,
    PageComponent,
    NewsPageComponent,
    PersianDatePipe,
    NewsListComponent,
    ProductListComponent,
    ProductPageComponent,
    LeftSideComponent,
    CategoryComponent,
    CartPageComponent,
    ValuesPipe,
    LoginComponent,
    RegisterComponent,
    PanelComponent,
    OrderListComponent,
    FactorComponent,
    PostComponent,
    ResultComponent,
    HomePanelComponent,
    MessagesComponent,
    ProfileComponent,
    ValidProfileComponent,
    ShowProfileComponent,
    EditProfileComponent,
    DatePicherComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    InfiniteScrollModule,
    LazyLoadImageModule,
    RouterModule.forRoot(appRoutes),
    MaterializeModule.forRoot(),
    StarRatingModule,
    Ng2Webstorage
  ],
  providers: [
    AppService, PersianCalendarServiceService,CartService,AuthService,AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
