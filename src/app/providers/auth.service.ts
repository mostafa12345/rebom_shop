import {AppConfig} from './../config/const.config';
import {Http, Headers, RequestOptions,URLSearchParams} from '@angular/http';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise'; // this adds the non-static 'toPromise' operator
import 'rxjs/add/operator/map';
declare function unescape(s : string) : string;
declare function escape(s : string) : string;

@Injectable()
export class AuthService {
  province : any[];
  constructor(private http : Http) {}

  public login(val:any) : Promise < any > {
    let body = new URLSearchParams();
    body.set("username", val.username);
    body.set("password", val.password);
    body.set("remember", val.remember);
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}); // ... Set content type to JSON
    let options = new RequestOptions({headers: headers});
    return new Promise(resolve => this.http.post(AppConfig.BASE_PATH + "/api/auth/login",body, options) // ...using post request
      .map(response => response.json()) // ...and calling .json() on the response to return data
      .subscribe(data => {
      resolve(data);
    }));
  }

  public register(user : any) : Promise < any > {
    let body = JSON.stringify(user); // Stringify payload
    let headers = new Headers({'Content-Type': 'application/json;charset=utf8'}); // ... Set content type to JSON
    let options = new RequestOptions({headers: headers}); // Create a request option

    return new Promise(resolve => this.http.post(AppConfig.BASE_PATH + "/api/auth/register", body, options) // ...using post request
      .map(response => response.json()) // ...and calling .json() on the response to return data
      .subscribe(data => {
      resolve(data);
    }));
  }

  public editProfile(user : any) : Promise < any > {
    let body = JSON.stringify(user); // Stringify payload
    let headers = new Headers({'Content-Type': 'application/json;charset=utf8'}); // ... Set content type to JSON
    headers.append("Authorization", "Bearer " + AppConfig.token());
    let options = new RequestOptions({headers: headers}); // Create a request option

    return new Promise(resolve => this.http.post(AppConfig.BASE_PATH + "/api/auth/edit", body, options) // ...using post request
      .map(response => response.json()) // ...and calling .json() on the response to return data
      .subscribe(data => {
      resolve(data);
    }));
  }

  getProvince() : Promise < any[] > {
    if(this.province) {
      return Promise.resolve(this.province);
    }
    return new Promise(resolve => this.http.get(AppConfig.BASE_PATH + "/api/auth/province") // ...using post request
      .map(response => response.json()) // ...and calling .json() on the response to return data
      .subscribe(data => {
      resolve(data);
    }));
  }
}
