import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AppService {

  homeData : any;
  categoies : any[];
  news : any[] = [];
  newsHasItems : boolean = true;
  productInCategory : any = {};
  products : any = {};
  newProduct : any[] = [];
  newProductHasItems : boolean = true;
  topHit : any[] = [];
  topHitHasItems : boolean = true;
  search : string;
  searchProducts : any[] = [];
  hasSearch : boolean = true;

  constructor(private http : Http) {}

  getHomeData() : Promise < any > {
    if(this.homeData) {
      return Promise.resolve(this.homeData);
    } else {
      return new Promise((resolve, reject) => {
        this
          .http
          .get("http://localhost:8080/marketing/api")
          .map(response => response.json())
          .subscribe(data => {
            this.homeData = data;
            resolve(this.homeData)
          });
      })
    }
  }

  getCategory(id : number) : Promise < any > {
    if(this.categoies) {
      for (var i = 0; i < this.categoies.length; i++) {
        if (this.categoies[i].id == id) {
          return Promise.resolve(this.categoies[i]);
        }
      }
    }
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/category/" + id)
        .map(response => response.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  getHomeNews(offset, limit) : Promise < any[] > {
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/news/" + offset + "/" + limit)
        .map(response => response.json())
        .subscribe(data => {
          resolve(data)
        });
    })
  }

  getNewsList(offset, limit) : Promise < any[] > {
    if(!this.newsHasItems) {
      return Promise.resolve(this.news);
    }
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/news/" + offset + "/" + limit)
        .map(response => response.json())
        .subscribe(data => {
          if (data.length < limit) {
            this.newsHasItems = false;
          }
          this.news = this
            .news
            .concat(data);
          resolve(this.news);
        });
    })
  }

  getProducts(id, offset, limit, search) : Promise < any[] > {
    if(!this.productInCategory[id]) {
      this.productInCategory[id] = {
        has: true,
        products: []
      }
    }
    if (!this.productInCategory[id].has) {
      return Promise.resolve(this.productInCategory[id].products);
    }
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/products/" + id + "/" + offset + "/" + limit)
        .map(response => response.json())
        .subscribe(data => {
          if (data.length < limit) {
            this.productInCategory[id].has = false;
          }
          this.productInCategory[id].products = this
            .productInCategory[id]
            .products
            .concat(data);
          resolve(this.productInCategory[id].products);
        });
    });
  }

  getSearch(offset, limit, search) : Promise < any[] > {
    if(this.search != search) {
      this.search = search;
      this.searchProducts = [];
      this.hasSearch = true;
    } else {
      if (!this.hasSearch) {
        return Promise.resolve(this.searchProducts);
      }
    }
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/products/-1/" + offset + "/" + limit + "?search=" + this.search)
        .map(response => response.json())
        .subscribe(data => {
          if (data.length < limit) {
            this.hasSearch = false;
          }
          this.searchProducts = this
            .searchProducts
            .concat(data);
          resolve(this.searchProducts);
        });
    })
  }

  getProduct(id) : Promise < any > {
    if(this.products[id]) {
      return Promise.resolve(this.products[id]);
    }
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/product/" + id)
        .map(response => response.json())
        .subscribe(data => {
          this.products[id] = data;
          resolve(this.products[id]);
        });
    })
  }

  getHomeCategories() : Promise < any[] > {
    if(this.categoies) {
      return Promise.resolve(this.categoies);
    } else {
      return new Promise((resolve, reject) => {
        this
          .http
          .get("http://localhost:8080/marketing/api/categories/")
          .map(response => response.json())
          .subscribe(data => {
            this.categoies = data;
            resolve(this.categoies);
          });
      })
    }
  }

  getHomeTopHit(offset, limit) : Promise < any[] > {
    if(!this.topHitHasItems) {
      return Promise.resolve(this.topHit);
    }
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/top-hit/" + this.topHit.length + "/" + limit)
        .map(response => response.json())
        .subscribe(data => {
          if (data.length < limit) {
            this.topHitHasItems = false;
          }
          this.topHit = this
            .topHit
            .concat(data);
          resolve(this.topHit)
        });
    })
  }

  getNewProduct(offset, limit) : Promise < any[] > {
    if(!this.newProductHasItems) {
      return Promise.resolve(this.newProduct);
    }
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/new-product/" + this.newProduct.length + "/" + limit)
        .map(response => response.json())
        .subscribe(data => {
          if (data.length < limit) {
            this.newProductHasItems = false;
          }
          this.newProduct = this
            .newProduct
            .concat(data);
          resolve(this.newProduct)
        });
    })
  }

  getPage(title) : Promise < any[] > {
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/page/" + title)
        .map(response => response.json())
        .subscribe(data => {
          resolve(data);
        });
    })
  }

  getNews(id) : Promise < any[] > {
    return new Promise((resolve, reject) => {
      this
        .http
        .get("http://localhost:8080/marketing/api/news/" + id)
        .map(response => response.json())
        .subscribe(data => {
          resolve(data);
        });
    })
  }

  sendMessage(contact : any) : Promise < any > {
    return new Promise((resolve, reject) => {
      this
        .http
        .post("http://localhost:8080/marketing/api/contact", JSON.stringify(contact), new RequestOptions({
          headers: new Headers({"Content-type": "application/json"})
        }))
        .map(response => response.json())
        .subscribe(data => {
          resolve(data)
        });
    })
  }
}
export interface productCollection {
  has : boolean;
  products : any[];
}
