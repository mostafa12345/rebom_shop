import {Injectable} from '@angular/core';
import {LocalStorage, LocalStorageService} from 'ngx-webstorage';

@Injectable()
export class CartService {
  @LocalStorage("carts")
  carts:any={};
  constructor(private session:LocalStorageService) {}

  addToCart(item:any){
    this.carts[item.product.id]=item;
    this.session.store("carts",this.carts);
  }

  removeFromCart(id:number){
    delete this.carts[id];
    this.session.store("carts",this.carts);
  }

  hasInCart(id:number):boolean{
    return this.carts[id];
  }
}
